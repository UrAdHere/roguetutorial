﻿using UnityEngine;
using System.Collections;
public class NoNewChildren : MonoBehaviour {
	public void OnTransformChildrenChanged(){
		
		foreach (Transform child in transform) {
			if (child.tag == "Card")
				child.SetParent (child.parent.parent);
		}
	}
}
