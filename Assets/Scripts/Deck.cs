﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Deck : MonoBehaviour {


	List<int> cards = new List<int>();

	void Start () {
		
		Shuffle ();
	}

	public void Shuffle(){
		int n = cards.Count;
		while (n > 1) {
			n--;
			int k = Random.Range (0, n + 1);
			int temp = cards [k];
			cards [k] = cards [n];
			cards [n] = temp;
		}
	}

	public int Pop(){
		int temp = cards [0];
		Debug.Log (temp);
		cards.RemoveAt (0);
		return temp;
	}

	public void Push(int[] contents){
		cards.AddRange (contents);
	}

	public bool IsEmpty(){
		return cards.Count == 0;
	}
}
