﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class CardZone : MonoBehaviour , IDropHandler{

	public bool valid;
	public Card cardPrefab;

	public void OnDrop(PointerEventData eventData){
		Debug.Log ("OnDrop to" + gameObject.name);

		Card d = eventData.pointerDrag.GetComponent<Card> ();
		if (d != null && valid) {
			d.SetLastParent(this.transform);
		}
	}
	public void OnPointerExit(PointerEventData eventData){

	}

	public void AddCard(Deck source){
		if (!source.IsEmpty()) {
			Card newCard = (Card)Instantiate(cardPrefab);
			newCard.transform.SetParent (this.transform);
			if (newCard.GetComponent<Card>() == null) {
				Debug.Log ("card Component not found");
			}
			newCard.GetComponent<Card>().SetCard(source.Pop ());
			newCard.transform.localScale = new Vector3 (1, 1, 1);
		} else {
			//TODO:Handling of Empty Deck Goes Here
		}
	}
		
}
