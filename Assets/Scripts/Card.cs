﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;



public class Card : MonoBehaviour, IBeginDragHandler ,IDragHandler, IEndDragHandler
{									  
	public TextAsset lookupFile;

	private int cardID;										//ID of card
	private CardLookup myLookup = new CardLookup();        
	private Transform myTitle;
	private Transform myText;

	private Transform lastParent = null;
	private Transform lastPlaceholder = null;


	GameObject placeholder = null;


	public void Awake(){

		myLookup.Load (lookupFile);
		this.SetCard (-1);
	}

	public void SetCard(int id){
		myTitle = this.transform.FindChild ("Title");
		myText = this.transform.FindChild ("Card Text");
		cardID = id;
		Debug.Log (cardID);
		if (cardID != -1) {
			myTitle.GetComponent<Text> ().text = myLookup.Find_ID (cardID.ToString ()).CardName;
			myText.GetComponent<Text> ().text = myLookup.Find_ID (cardID.ToString ()).CardText;
		}
	}


    public void OnBeginDrag(PointerEventData eventData)
    {
        Debug.Log("OnBeginDrag");

		placeholder = new GameObject ();
		placeholder.transform.SetParent (this.transform.parent);
		LayoutElement le = placeholder.AddComponent<LayoutElement> ();
		le.preferredWidth = this.GetComponent<LayoutElement> ().preferredWidth;
		le.preferredHeight = this.GetComponent<LayoutElement> ().preferredHeight;
		le.flexibleWidth = 0;
		le.flexibleHeight = 0;
		placeholder.transform.SetSiblingIndex( this.transform.GetSiblingIndex());

		lastParent = this.transform.parent;


		FindValidParent ();
		GetComponent<CanvasGroup>().blocksRaycasts = false;
    }

    public void OnDrag(PointerEventData eventData)
    {
        this.transform.position = eventData.position;

		int newSiblingIndex = lastParent.childCount;

		for(int i=0; i < lastParent.childCount; i++){
			if (this.transform.position.x < lastParent.GetChild (i).position.x) {
				newSiblingIndex = i;

				if (placeholder.transform.GetSiblingIndex () < newSiblingIndex)
					newSiblingIndex--;
				break;
			}

		}
		placeholder.transform.SetSiblingIndex (newSiblingIndex);
    }
    public void OnEndDrag(PointerEventData eventData)
    {
        Debug.Log("OnEndDrag");
		this.transform.SetParent (lastParent);
		GetComponent<CanvasGroup> ().blocksRaycasts = true;
		Destroy (placeholder);
		this.transform.SetSiblingIndex(placeholder.transform.GetSiblingIndex());
    }

	public void SetLastParent(Transform parent){
		lastParent = parent;
	}

	private void FindValidParent (){
		this.transform.SetParent (this.lastParent.parent);
	}
		
}
 