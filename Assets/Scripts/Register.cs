﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Register : MonoBehaviour , IDropHandler {

	private Transform myLabel;
	
	public void OnDrop(PointerEventData eventData){
		Debug.Log ("OnDrop to" + gameObject.name);

		Card d = eventData.pointerDrag.GetComponent<Card> ();
		if (d != null &&  this.transform.FindChild("Card") == null) {
			d.SetLastParent (this.transform);

		}
	}

	public void UpdateLabel(int myNumber){
		myLabel = this.transform.FindChild("Label");
		myLabel.GetComponent<Text> ().text= "Register " + myNumber;
	}


}